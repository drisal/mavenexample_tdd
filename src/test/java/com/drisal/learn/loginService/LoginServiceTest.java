package com.drisal.learn.loginService;

import org.junit.Test;
import static org.mockito.Mockito.*;


public class LoginServiceTest {

	@Test
	public void itShouldSetAccountToLoggedInWhilePasswordMatches() {
		IAccount account=mock(IAccount.class);
		when(account.passwordMatches(anyString())).thenReturn(true);
		
		IAccountRepository accountRepository= mock(IAccountRepository.class);
		when(accountRepository.find(anyString())).thenReturn(account);
		
		LoginService service=new LoginService(accountRepository);
		service.login("brett","password");
		verify(account,times(1)).setLoggedIn(true);
	}

}
