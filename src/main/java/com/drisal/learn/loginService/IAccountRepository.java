package com.drisal.learn.loginService;

public interface IAccountRepository {

	IAccount find(String accountId);
}
