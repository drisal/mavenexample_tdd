package com.drisal.learn.loginService;

public interface IAccount {

	void setLoggedIn(boolean value);
	boolean passwordMatches(String candidate);
}
